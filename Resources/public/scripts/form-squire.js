(function () {
var addListeners = function (element) {
    [].forEach.call(element.querySelectorAll('iframe.squire'), function (iframe) {
        if (iframe.dataset.initialized) {
            return;
        }
        iframe.dataset.initialized = true;
        var textarea = iframe.parentNode.querySelector('textarea');
        var buttons = iframe.parentNode.querySelectorAll('button[data-action]');
        var editor;
        iframe.addEventListener('load', function () {
            if (textarea.offsetWidth) {
                iframe.style.width = textarea.offsetWidth + 'px';
            }
            if (textarea.offsetHeight) {
                iframe.style.height = textarea.offsetHeight + 'px';
            }
            textarea.style.display = 'none';
            editor = iframe.contentWindow.editor;
            editor.setHTML(textarea.value);
            editor.addEventListener('blur', function () {
                textarea.value = editor.getHTML();
            });
            [].forEach.call(buttons, function (button) {
                button.addEventListener('click', function () {
                    if (button.dataset.tag) {
                        if (editor.hasFormat(button.dataset.tag)) {
                            editor[button.dataset.removeAction]();
                        } else {
                            editor[button.dataset.action]();
                        }
                    } else {
                        editor[button.dataset.action]();
                    }
                });
            });
        }, false);
    });
};

addListeners(document);
var mo = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        addListeners(mutation.target);
    });
});
mo.observe(document, {childList: true, subtree: true});
})();
