<?php

namespace AdrienSamson\FormExtraBundle;

use Symfony\Component\DomCrawler\Crawler;

class HtmlFilter
{
    private $config = [];

    public function __construct($config)
    {
        $this->config = $config;
        foreach ($this->config as $tagName => $attrs) {
            if ($attrs) {
                $this->config[strtolower($tagName)] = array_map('strtolower', $attrs);
            } else {
                $this->config[strtolower($tagName)] = [];
            }
        }
    }

    public function filter($html)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($html);

        $resultDocument = new \DOMDocument();
        $parentElement = $resultDocument->createElement('body');
        $resultDocument->appendChild($parentElement);

        foreach ($crawler as $child) {
            $this->filterChildren($child, $parentElement);
        }

        return substr(html_entity_decode($resultDocument->saveHTML(), ENT_QUOTES, "UTF-8"), 6, -8);
    }

    private function filterNode(\DOMNode $node, \DOMNode $destination)
    {
        if ($node instanceof \DOMText) {
            $el = $destination->ownerDocument->createTextNode($node->textContent);
            $destination->appendChild($el);
        } elseif ($node instanceof \DOMElement) {
            $tagName = strtolower($node->tagName);
            if (in_array($tagName, array_keys($this->config))) {
                $el = $destination->ownerDocument->createElement($tagName);
                $destination->appendChild($el);
                foreach ($node->attributes as $attribute) {
                    /* @var \DOMAttr $attribute */
                    $attrName = strtolower($attribute->name);
                    if (in_array($attrName, $this->config[$tagName])) {
                        $el->setAttribute($attrName, $attribute->value);
                    }
                }
                $this->filterChildren($node, $el);
            } else {
                $this->filterChildren($node, $destination);
            }
        }
    }

    private function filterChildren(\DOMNode $element, \DOMNode $destination)
    {
        foreach ($element->childNodes as $child) {
            /* @var \DOMElement $child */
            $this->filterNode($child, $destination);
        }
    }
}
