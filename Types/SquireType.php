<?php

namespace AdrienSamson\FormExtraBundle\Types;

use AdrienSamson\FormExtraBundle\HtmlFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SquireType extends AbstractType
{
    public function getParent()
    {
        return TextareaType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allowed_tags' => [
                'p' => null,
                'br' => null,
                'b' => null,
                'i' => null,
                'u' => null,
            ]
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $htmlFilter = new HtmlFilter($options['allowed_tags']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $formEvent) use ($htmlFilter) {
            $formEvent->setData($htmlFilter->filter($formEvent->getData()));
        });
    }
}
