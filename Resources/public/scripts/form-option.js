(function () {
var addListeners = function (element) {
    [].forEach.call(element.querySelectorAll('.form-option'), function (parent) {
        if (parent.dataset.initialized) {
            return;
        }
        parent.dataset.initialized = true;
        var nullCheckbox = parent.querySelector('[data-null] input[type=checkbox]');
        var child = parent.querySelector('[data-child]');

        function setVisibility() {
            child.style.display = nullCheckbox.checked ? 'none' : 'block';
        }
        nullCheckbox.addEventListener('change', setVisibility);
        setVisibility();
    });
};

addListeners(document);
var mo = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        addListeners(mutation.target);
    });
});
mo.observe(document, {childList: true, subtree: true});
})();
