<?php

namespace AdrienSamson\FormExtraBundle\Types;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionType extends AbstractType implements DataTransformerInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('null', CheckboxType::class, ['label' => $options['null_label'], 'required' => false])
            ->add('child', $options['type'], array_merge($options['options'], [
                'required' => false,
                'label' => false,
            ]));
        $builder->addViewTransformer($this);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['type']);
        $resolver->setDefaults([
            'options' => [],
            'null_label' => 'None',
        ]);
    }

    public function transform($value)
    {
        return [
            'null' => $value === null,
            'child' => $value,
        ];
    }

    public function reverseTransform($value)
    {
        if ($value === null) {
            return null;
        }
        if ($value['null']) {
            return null;
        }

        return $value['child'];
    }
}
